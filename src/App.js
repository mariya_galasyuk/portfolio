import React from 'react';
import Projects from './Projects';
import Contacts from './Contacts';
import Home from './Home';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
function responsive() {
 
}

class App extends React.Component {
  componentDidMount(){
    let x = document.getElementById("myTopnav");
    if (x.className === "topnav full-width") {
      x.className += " responsive";
    } else {
      x.className = "topnav full-width";
    }
  }
  render(){
    
  return (
    <Router>
      <div>
        {/* <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/projects">Projects</Link>
            </li>
            <li>
              <Link to="/contacts">Contacts</Link>
            </li>
          </ul>
        </nav> */}
<div className="topnav full-width" id={"myTopnav"}>
  {/* <div className="background-angle"></div> */}
  <a href="/" className="active">HOME</a>
  <a href="/projects">PROJECTS</a>
  <a href="/contacts">CONTACTS</a>
  <a href="javascript:void(0);" className="icon" onClick={responsive()}>
    <FontAwesomeIcon icon={faBars} />
  </a>
</div>
      
        <Switch>
          <Route path="/projects">
            <Projects />
          </Route>
          <Route path="/contacts">
            <Contacts />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
      }
}

export default App;
